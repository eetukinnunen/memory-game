import React from 'react';

const Card = (props) => {
    const {id, pair, handleClick, style} = props;
    return (
        <div
            className="card-wrapper"
            style={style}
        >
            <div
                className="card"
                onClick={(e) => handleClick(id, pair, e.target)}
            />
        </div>
    )
}

export default Card;