import React, {useState, useEffect, useRef} from 'react';
import Card from './card.js';

const Game = (props) => {
    const [cardArray, setCardArray] = useState([]);
    const firstCardSelected = useRef(false);
    const clickDisabled = useRef(false);
    const previousCard = useRef({});
    const completedPairs = useRef(0); // how many pairs have been completed
    const clickCounter = useRef(0); // the amount of times the player has clicked

    const {pairs} = props;
    const cardAmount = pairs * 2;

    const logEverything = () => {
        console.log("cardArray", cardArray);
        console.log("cardArray length", cardArray.length);
        console.log("firstCardSelected", firstCardSelected);
        console.log("clickDisabled", clickDisabled);
        console.log("previousCard", previousCard);
        console.log("completedPairs", completedPairs);
        console.log("clickCounter", clickCounter);
    };

    const cardShuffle = (array) => {
        // Fisher-Yates shuffle

        let counter = array.length;
        // While there are elements in the array
        while (counter > 0) {
            // Pick a random index
            let index = Math.floor(Math.random() * counter);
            // Decrease counter by 1
            counter--;
            // And swap the last element with it
            let temp = array[counter];
            array[counter] = array[index];
            array[index] = temp;
        }
        return array;
    };

    const calculateCardStyle = () => {
        // card size: 22 x 32 -> 11:16

        //todo: determine automatically based on pair count
        const perRow = 4;

        //todo: no magic numbers
        // 32/22: background-img dimensions
        // 500: game area width
        // 2.5: inner margins
        // 6: border
        const height = (32 / 22) * (500 / perRow) - 2.5 - 6;
        
        const cardStyle = {
            flex: `1 0 ${100/perRow}%`,
            height
        };

        console.log(cardStyle);

        return cardStyle;
    };

    const printCards = () => {
        const cardStyle = calculateCardStyle();
        
        // Give each pair a number, shuffle them in the array
        let tempArr = [];
        for (let i = 1; i <= cardAmount; i++) {
            // Determine current pair number by halving the iterator and rounding up 
            //  -> 1.5 & 2 are both pair 2
            // So for every two passes of this loop, a pair is created
            let pair = Math.ceil(i / 2);

            tempArr.push({
                id: i, // the unique number of the card
                pair: pair, // which pair this card belongs to (not unique),
                content:
                    <Card 
                        id={i}
                        pair={pair}
                        handleClick={handleClick}
                        style={cardStyle}
                        key={`${i}-${new Date().getTime()}`} // simply using i isn't enough or reset breaks
                    /> // the html element representing this card
            });
        }
        setCardArray(cardShuffle([...tempArr]));
    };

    useEffect(printCards, []);

    const handleClick = (id, pair, clickedCardElement) => {
        logEverything();

        if (!clickDisabled.current) { // prevents clicks during a non-pair of cards being open

            if (firstCardSelected.current === false) { // the first card click of a pair
                firstCardSelected.current  = true; // the first of 2 cards is open
                previousCard.current  = {
                    id,
                    pair,
                    content: clickedCardElement
                }; // set this card as the last clicked one

                clickedCardElement.style.backgroundImage = `url("/img/pair-${pair}.png")`;

                console.table(cardArray);
                const clickedCard = cardArray.find(card => card.id === id);
                if (clickedCard) {
                    console.log("card found from array");
                }

                clickCounter.current++;

            } else if (id !== previousCard.current.id) { // on second card click, if not clicking the same card twice
                firstCardSelected.current = false; // reset
                clickedCardElement.style.backgroundImage = `url("/img/pair-${pair}.png")`;
                clickCounter.current++;

                if (pair !== previousCard.current.pair) { // if not a pair
                    clickDisabled.current = true;

                    // Show card faces for 2 seconds, then turn them again
                    setTimeout(function() {
                        clickedCardElement.style.backgroundImage = 'url("/img/spade.png")';
                        previousCard.current.content.style.backgroundImage = 'url("/img/spade.png")';
                        clickDisabled.current = false;
                    }, 2000);

                } else { // is a pair
                    completedPairs.current++;
                    if (completedPairs.current === pairs) {
                        window.alert(`You are winner. Your score is ${clickCounter.current}
                                    (the lower the better).`);
                    }
                }
            } // if clicking the same card twice, ignore
        }
    };

    const resetGame = () => {
        logEverything();

        // Reset values to initials
        setCardArray([]);
        firstCardSelected.current = false;
        clickDisabled.current = false;
        previousCard.current = {};
        completedPairs.current = 0;
        clickCounter.current = 0;

        printCards();
    };

    return (
        <div id="game-area">
            <div id="card_grid">
                {cardArray.map(card => card.content)}
            </div>
            <button id="reset" onClick={resetGame}>Reset</button>
        </div>
    )
};

export default Game;