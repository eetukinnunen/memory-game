import React from 'react';
import ReactDOM from 'react-dom';
import Game from './game.js';
import './style.css';

const App = () => {
  return (
    <div>
      <h1>Memory game</h1>
      <Game pairs={8} /> {/* there are only 8 images, so limit pairs to maximum of 8 */}
    </div>
  )
}

ReactDOM.render(
    <React.StrictMode>
        <App />
    </React.StrictMode>,
    document.getElementById('root')
);
